import {
  SELECT_SUBREDDIT,
  INVALIDATE_SUBREDDIT,
  REQUEST_POST,
  RECEIVE_POST
} from '../actions/actionTypes'
import { combineReducers } from 'redux'
/**
 * 
 * The following is the shape of the app state 
    * {
          selectedSubreddit: 'frontend',
          postsBySubreddit: {
            frontend: {
              isFetching: true,
              didInvalidate: false,
              items: []
            },
            reactjs: {
              isFetching: false,
              didInvalidate: false,
              lastUpdated: 1439478405547,
              items: [
                {
                  id: 42,
                  title: 'Confusion about Flux and Relay'
                },
                {
                  id: 500,
                  title: 'Creating a Simple Application Using React JS and Flux Architecture'
                }
              ]
            }
          }
        }
 */

const selectedSubreddit = (state = 'react', action) => {
  switch (action.type) {
    case SELECT_SUBREDDIT:
      return action.subreddit
    default:
      return state
  }
}

const posts = (
  state = {
    isFetching: false,
    didInvalidate: false,
    items: []
  },
  action
) => {
  switch (action.type) {
    case INVALIDATE_SUBREDDIT:
      return { ...state, didInvalidate: true }
    case REQUEST_POST:
      return { ...state, isFetching: true, didInvalidate: false }
    case RECEIVE_POST:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        lastUpdated: action.receivedAt,
        items: action.posts
      }
    default:
      return state
  }
}

const postsBySubreddit = (state = {}, action) => {
  switch (action.type) {
    case INVALIDATE_SUBREDDIT:
    case REQUEST_POST:
    case RECEIVE_POST:
      return {
        ...state,
        [action.subreddit]: posts(state[action.subreddit], action)
      }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  selectedSubreddit,
  postsBySubreddit
})

export default rootReducer
