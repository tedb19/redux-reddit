import {
  SELECT_SUBREDDIT,
  INVALIDATE_SUBREDDIT,
  REQUEST_POST,
  RECEIVE_POST
} from './actionTypes'
import fetch from 'cross-fetch'

export const selectSubreddit = subreddit => {
  return {
    type: SELECT_SUBREDDIT,
    subreddit
  }
}

export const invalidateSubreddit = subreddit => {
  return {
    type: INVALIDATE_SUBREDDIT,
    subreddit
  }
}

export const requestPosts = subreddit => {
  return {
    type: REQUEST_POST,
    subreddit
  }
}

export const receivePosts = (subreddit, json) => {
  console.log('json', json)
  return {
    type: RECEIVE_POST,
    subreddit,
    posts: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}
/**
 * Through redux-thunk, we are able to return async functions as
 *  opposed to normal actions from action creators.
 *
 * The returned function takes dispatch and (optionally) getState as
 * parameters
 *
 */
export const fetchPosts = subreddit => {
  /**
   * Thunk middleware knows how to handle functions.
   * It passes the dispatch method as an argument to the function,
   * thus making it able to dispatch actions itself.
   */

  return dispatch => {
    /**
     * We can dispatch as many times as we want!
     * First dispatch: the app state is updated to inform
     * that the API call is starting.
     */
    dispatch(requestPosts(subreddit))

    /**
     * Do not use catch, because that will also catch any
     * errors in the dispatch and resulting render,
     * causing a loop of 'Unexpected batch number' errors.
     */
    return fetch(`https://www.reddit.com/r/${subreddit}.json`)
      .then(
        response => response.json(),
        error => console.log(`Error occured ${error}`)
      )
      .then(
        json => {
          // Here, we update the app state with the results of the API call.
          dispatch(receivePosts(subreddit, json))
        },
        error => console.log(`Error occured ${error}`)
      )
  }
}

export const shouldFetchPosts = (state, subreddit) => {
  const posts = state.postsBySubreddit[subreddit]

  if (!posts) {
    return true
  } else if (posts.isFetching) {
    return false
  } else {
    return posts.didInvalidate
  }
}

export const fetchPostsIfNeeded = subreddit => {
  return (dispatch, getState) => {
    if (shouldFetchPosts(getState(), subreddit)) {
      return dispatch(fetchPosts(subreddit)) // dispatching a thunk from a thunk! :)
    } else {
      // Let the caller know that there's nothing to wait for
      return Promise.resolve()
    }
  }
}
